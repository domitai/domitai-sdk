require('dotenv').config();
const debug = require('debug')('domitai-sdk');
const IO = require('socket.io-client');
const request = require('superagent');

const URL = process.env.URL || 'https://domitai.com';

const socket = IO(URL, {reconnectionDelayMax: 10000});

socket.on('connect', () => {
  debug('Connected to Domitai');
});

socket.on('disconnect', () => {
  debug('Reconecting to Domitai');
});

socket.on('message', data => {
  console.log('DATA', data);
  //if(data.payload==='pong') setTimeout(()=>socket.emit('message','ping'),1000);
});

socket.on('subs', data => {
  console.log('Subscripcion', data);
});

socket.emit('message', 'hola');

console.log('Enviando GET');
socket.emit('get', {path: '/api'}, answer => {
  console.log('La llamada de api trae', answer);
});

request.get(URL + '/api').then(res => console.log(res.body));
